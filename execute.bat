@echo off

echo ### STARTING ###

set TD_CONFIG=development
set TD_PROJECT_NAME=td_project_template
set TD_HASH=_BUILD_NUMBER_TO_COMPLETE_

REM set TD_LOG_SUFFIX=''

REM set TOUCH_QUICK_CRASH=1
REM set TOUCH_DISABLE_CRASH_HANDLERS=1
REM set TOUCH_FAST_LOAD=1
REM set TOUCH_NO_UPDATE_CHECK=1

echo Starting %TD_PROJECT_NAME% as %TD_CONFIG%

timeout /t 1

"%~dp0\01_TOUCHDESIGNER\%TD_HASH%\bin\TouchDesigner099.exe" "%~dp0\%TD_PROJECT_NAME%.toe"
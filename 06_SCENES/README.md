# Scenes folder

*The scenes folder is not tracked in that repository.*

Only 01_Scene is tracked as a very lightweight example. Please import your scenes using the alternative methods.
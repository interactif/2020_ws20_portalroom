"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions

import os
import json
from shutil import copyfile

class root:
	"""
	root description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached

		print('.o0O === ROOT INIT START === O0o.')

		self.ownerComp = ownerComp
		
		self.projectMode = ''

		if 'TD_CONFIG' in os.environ:
			self.projectMode = os.environ['TD_CONFIG']
		else:
			self.projectMode = 'development'

		TDF.createProperty(self,
				'Mode', # name
				value=self.projectMode, # starting value
				dependable=True, # dependable (True, False or "deep")
				readOnly=True) # write via self._MyProperty.val

		realFileName = project.name.split('.')[0]
		configPath = project.folder + '/' + realFileName + '.' + self.projectMode + '.config.json'

		try:
			with open(configPath, 'r') as f:
				self.projectConfig = json.load(f)
		except:
			print("Config file " + configPath + ' does not exist, creating it out of template file.')

			source = project.folder + '/' + realFileName + '.template.config.json'
			target = configPath

			# adding exception handling
			try:
				copyfile(source, target)
				print("Copy of template file created as " + configPath + '.')
				try:
					with open(configPath, 'r') as f:
						self.projectConfig = json.load(f)
				except:
					print("Terminate, could not open config file created from template.")		
			except IOError as e:
				print("Unable to copy file. %s" % e)
			except:
				print("Unexpected error: " + sys.exc_info())

		TDF.createProperty(self,
				'Config', # name
				value=self.projectConfig, # starting value
				dependable=True, # dependable (True, False or "deep")
				readOnly=True) # write via self._MyProperty.val

		print('.o0O === ROOT INIT END === O0o.')

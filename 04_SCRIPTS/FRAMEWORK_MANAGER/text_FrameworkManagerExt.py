"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions

import os
import json

class FrameworkManagerExt:
	"""
	FrameworkManagerExt description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp
		
		parent().par.Mode = root.Mode.capitalize()

		TDF.createProperty(self,
				'Inputs', # name
				value=root.Config['Inputs'], # starting value
				dependable="deep", # dependable (True, False or "deep")
				readOnly=True) # write via self._MyProperty.val	
		
		if root.Mode == None or root.Mode.capitalize() != 'Production':
			parent().par.Debugmodes = None
			parent().par.Debugmodes.enable = True
		else:
			parent().par.Debugmodes.enable = False

		"""
		INPUTS / OUTPUTS
		"""
		self.xagoraToolkit = op.XAGORA

		# Trigger on init, can retrigger anytime from UI after changes in JSON file.
		self.UpdateSystemConfigFromFile(root.Config['System Settings'])
		self.UpdateXagoraConfigFromFile(root.Config['XAgora Settings'])
		

	"""
	SETTINGS
	"""
	def UpdateSystemConfigFromFile(self, jsonConfig):
		op.WINDOW.par.drawwindow = jsonConfig['Update window']
		op.WORKSTATION_VIEWER.par.w = jsonConfig['Perform resolution'][0]
		op.WORKSTATION_VIEWER.par.h = jsonConfig['Perform resolution'][1]

	def UpdateXagoraConfigFromFile(self, jsonConfig):
		# Get settings from file and set X-Agora Toolkit
		self.xagoraToolkit.par.Brokerhost = jsonConfig['Ip'] + ':' + str(jsonConfig['Port'])
		self.xagoraToolkit.par.Datastorehost = jsonConfig['Hostname']
		self.xagoraToolkit.par.Projectidentifier = jsonConfig['Identifier']
		self.xagoraToolkit.par.Catalogname = jsonConfig['Catalog']
		self.xagoraToolkit.par.Reconnect.pulse()
		self.xagoraToolkit.par.Pushtasks.pulse()
		return			
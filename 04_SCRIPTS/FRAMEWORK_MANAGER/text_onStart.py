# me - this DAT
# 
# frame - the current frame
# state - True if the timeline is paused
# 
# Make sure the corresponding toggle is enabled in the Execute DAT.

def onStart():
	""" 
	=== INIT SEQUENCE ===
	
	In that fonction, place any code that you want to execute on start, such as Initialization of your extensions
	"""

	# Trigger init of UTILITIES Ext
	op.LOGGER.Info("=== Execute onStart() INIT SEQUENCE ===")
	
	# Do any additional INIT routines here.

	op.LOGGER.Info("=== End of onStart() INIT SEQUENCE ===")
	return
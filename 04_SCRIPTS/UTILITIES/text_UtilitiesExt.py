"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
TDJ = op.TDModules.mod.TDJSON

import os
import sys
import json
import re

import tdutils
import tdutils.outputcatcher

class UtilitiesExt:
	"""
	UtilitiesExt description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp

	"""
	FRAMEWORK HELPERS
	"""
	def LoadJSON(self, path, key=None):
		jsonObj = None
		with open(path, 'r') as f:
			jsonObj = json.load(f)
		if key != None:
			return jsonObj[key]
		else:
			return jsonObj

	def VerifyFileExist(self, path, raiseError=True):
		if raiseError:
			try:
				fp = open(path)
			except PermissionError:
				op.LOGGER.Error('Could not access file ' + path) if op.LOGGER.extensionsReady else print('Could not access file ' + path)
				return False
			else:
				with fp:			
					return True
		else:
			try:
				fp = open(path)
			except:
				op.LOGGER.Error('Could not access file ' + path) if op.LOGGER.extensionsReady else print('Could not access file ' + path)
				return False
			else:
				with fp:			
					return True	

	def GetCustomPageFromOp(self, op, pageName):
		for page in op.customPages:
			if page.name == pageName:
				return page
			else:
				continue
		return None

	def IsListEmpty(self, genericList):
		if len(genericList) > 0:
			return False
		else:
			return True

	def GetResourceStringFilePath(self, toxPath = None):
		resourcePath = None
		if toxPath == None:	
			resourcePath = project.folder + '/resources.json'
		else:
			toxPath = os.path.dirname(toxPath)
			resourcePath = toxPath + '/01_MEDIAS/resources.json'
		return resourcePath

	def GetResourceString(self, key, language='EN', path=None):
		filePath = self.GetResourceStringFilePath(path)
		if self.VerifyFileExist(filePath):
			jsonObj = self.LoadJSON(filePath, key)
			return jsonObj[language]	
		
	def GetFolder(self, pathWithFile):
		return os.path.dirname(pathWithFile)

	def CleanString(self, stringToClean):
		"""
		Takes a string with non valid characters and return it as alpha-num
		"""
		pattern = re.compile('[\W_]+')
		return pattern.sub('', stringToClean)

	def ArrayAsStringToArray(self, arrayAsString):
		"""
		Takes a string that represent an array and turns it back to an array
		"""
		pattern = re.compile('[\W_]+')
		toArray = [pattern.sub('', x) for x in arrayAsString.split(',')]
		return toArray	

	def GetJsonConfig(self, mode=None, key=None):
		# Load settings from JSON
		realFileName = project.name.split('.')[0]
		configPath = project.folder + '/' + realFileName + '.config.json' if mode==None else project.folder + '/' + realFileName + '.' + mode + '.config.json'

		if self.VerifyFileExist(configPath):
			jsonFileAsJsonObj = self.LoadJSON(configPath, key)		
			return jsonFileAsJsonObj

	def GetProjectMode(self):
		projectMode = ''

		if 'TD_CONFIG' in os.environ:
			projectMode = os.environ['TD_CONFIG']
		else:
			projectMode = 'development'

		return projectMode

"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
TDJ = op.TDModules.mod.TDJSON

import datetime

class BananaGitExt:
	"""
	BananaGitExt description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp

		self.ownerComp.par.Releaseversionnumber = op('version').text

	def SaveExternals(self):
		if parent().par.Active:
			# get datetime
			op.LOGGER.Info('==================================================')
			op.LOGGER.Info('Presave process started ' + str(datetime.datetime.now()))
			
			# get project root
			rootComps = root.findChildren(type=COMP)
			for comp in rootComps:
				#self.GetParametersAsJson(comp)
				#if comp.dirty:
				op.LOGGER.Info('We will now try to save ' + comp.name)
				try:
					# Get comp file path
					filePath = comp.par.externaltox
					if filePath != '':
						comp.save(filePath)
						op.LOGGER.Info(comp.name + ' was saved in ' + filePath + '.')
					else:
						# With the current setup, it should never hit that op.LOGGER.Info, we need to check for "if dirty" later.
						op.LOGGER.Warning('Component was never saved as tox, please safe and specify a path.')	
				except:
					op.LOGGER.Error('We couldn\'t save ' + comp.name)
						
			op.LOGGER.Info('Presave process ended ' + str(datetime.datetime.now()))
			op.LOGGER.Info('===============================================')

	def GetParametersAsJson(self, comp):
		# op.LOGGER.Info(TDJ.serializeTDData(comp, verbose=True))
		# Not available === ['exportOP','exportSource','bindMaster','tuplet','prevMode','owner']
		extraAttrs = ['valid',
					'val',
					'expr',
					'bindExpr',
					'bindReferences',
					'index',
					'vecIndex',
					'name',
					'label',
					'startSection',
					'displayOnly',
					'readOnly',
					'tupletName',
					'min',
					'max',
					'clampMin',
					'clampMax',
					'default',
					'defaultExpr',
					'normMin',
					'normMax',
					'normVal',
					'enable',
					'order',
					'page',
					'password',
					'mode',
					'menuNames',
					'menuLabels',
					'menuIndex',
					'menuSource']
		# op.LOGGER.Info(TDJ.opToJSONOp(comp, extraAttrs=extraAttrs, forceAttrLists=False, includeCustomPages=True, includeBuiltInPages=True))
		op.LOGGER.Info(TDJ.opToJSONOp(comp, extraAttrs=extraAttrs, forceAttrLists=False))

	def GenerateReleaseBuild(self):				
		# Reset view
		p = ui.panes.current
		p.owner = root
		p = p.changeType(PaneType.NETWORKEDITOR) 

		# Save current state, including externals
		project.save()

		# The anwer will return the button index. A Continue action is therefore a 0
		bananaWarning = ui.messageBox('BananaGit', 'BananaGit will be disabled, do you wish to continue ?', buttons=['Continue', 'Cancel'])
		
		if bananaWarning == 0:
			# Go through every externals and backup
			for oOp in root.findChildren(tags=['BananaGit'], onlyNonDefaults=True):
				if oOp.isDAT:
					oOp.par.syncfile = False
					oOp.par.file = ''

				elif oOp.isCOMP:
					oOp.par.externaltox = ''

			parent().par.Active = False
			parent().par.Active.readOnly = True
			op.LOGGER.Info('BananaGit is now deactivated.')
			
			# Cache original serialized file
			originalProjectToe = project.folder + '/' + project.name
			
			# Create Release
			releaseFilePath = project.folder + '/_RELEASE/' + parent().par.Releasefilename.eval()
			project.save(releaseFilePath, saveExternalToxs=False)
			
			# Reload serialized project
			project.load(originalProjectToe)

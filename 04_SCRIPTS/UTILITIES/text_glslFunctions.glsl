//These are all cool functions for you to use in glsl with your projects :)
//Use : #include </project/UTILITIES/glslFunctions> to use this script from anywhere in your project

//
//Compositing functions
//

float blendOverlay(float base, float blend) {
	return base<0.5?(2.0*base*blend):(1.0-2.0*(1.0-base)*(1.0-blend));
}

vec4 blendOverlay(vec4 base, vec4 blend) {
	return vec4(blendOverlay(base.r,blend.r),blendOverlay(base.g,blend.g),blendOverlay(base.b,blend.b), blendOverlay(base.a,blend.a));
}
vec4 blendAdd(vec4 base, vec4 blend) {
	return min(base+blend,vec4(1.0));
}
vec4 blendAverage(vec4 base, vec4 blend) {
	return (base+blend)/2.0;
}
float blendColorBurn(float base, float blend) {
	return (blend==0.0)?blend:max((1.0-((1.0-base)/blend)),0.0);
}

vec4 blendColorBurn(vec4 base, vec4 blend) {
	return vec4(blendColorBurn(base.r,blend.r),blendColorBurn(base.g,blend.g),blendColorBurn(base.b,blend.b), blendColorBurn(base.a,blend.a));
}

vec4 blendDifference(vec4 base, vec4 blend) {
	return abs(base-blend);
}
float blendReflect(float base, float blend) {
	return (blend==1.0)?blend:min(base*base/(1.0-blend),1.0);
}

vec4 blendReflect(vec4 base, vec4 blend) {
	return vec4(blendReflect(base.r,blend.r),blendReflect(base.g,blend.g),blendReflect(base.b,blend.b), blendReflect(base.a,blend.a));
}

vec4 blendGlow(vec4 base, vec4 blend) {
	return blendReflect(blend,base);
}

vec4 blendHardLight(vec4 base, vec4 blend) {
	return blendOverlay(blend,base);
}
float blendLighten(float base, float blend) {
	return max(blend,base);
}

vec4 blendLighten(vec4 base, vec4 blend) {
	return vec4(blendLighten(base.r,blend.r),blendLighten(base.g,blend.g),blendLighten(base.b,blend.b), blendLighten(base.a,blend.a));
}

vec4 blendMultiply(vec4 base, vec4 blend) {
	return base*blend;
}
vec4 blendNegation(vec4 base, vec4 blend) {
	return vec4(1.0)-abs(vec4(1.0)-base-blend);
}
float blendDarken(float base, float blend) {
	return min(blend,base);
}

vec4 blendDarken(vec4 base, vec4 blend) {
	return vec4(blendDarken(base.r,blend.r),blendDarken(base.g,blend.g),blendDarken(base.b,blend.b), blendDarken(base.a,blend.a));
}

float blendPinLight(float base, float blend) {
	return (blend<0.5)?blendDarken(base,(2.0*blend)):blendLighten(base,(2.0*(blend-0.5)));
}

vec4 blendPinLight(vec4 base, vec4 blend) {
	return vec4(blendPinLight(base.r,blend.r),blendPinLight(base.g,blend.g),blendPinLight(base.b,blend.b), blendPinLight(base.a,blend.a));
}
float blendScreen(float base, float blend) {
	return 1.0-((1.0-base)*(1.0-blend));
}

vec4 blendScreen(vec4 base, vec4 blend) {
	return vec4(blendScreen(base.r,blend.r),blendScreen(base.g,blend.g),blendScreen(base.b,blend.b), blendScreen(base.a,blend.a));
}
float blendSoftLight(float base, float blend) {
	return (blend<0.5)?(2.0*base*blend+base*base*(1.0-2.0*blend)):(sqrt(base)*(2.0*blend-1.0)+2.0*base*(1.0-blend));
}

vec4 blendSoftLight(vec4 base, vec4 blend) {
	return vec4(blendSoftLight(base.r,blend.r),blendSoftLight(base.g,blend.g),blendSoftLight(base.b,blend.b),blendSoftLight(base.a,blend.a));
}
float blendSubstract(float base, float blend) {
	return max(base+blend-1.0,0.0);
}

vec4 blendSubstract(vec4 base, vec4 blend) {
	return max(base+blend-vec4(1.0),vec4(0.0));
}
float blendColorDodge(float base, float blend) {
	return (blend==1.0)?blend:min(base/(1.0-blend),1.0);
}

vec4 blendColorDodge(vec4 base, vec4 blend) {
	return vec4(blendColorDodge(base.r,blend.r),blendColorDodge(base.g,blend.g),blendColorDodge(base.b,blend.b), blendColorDodge(base.a,blend.a));
}
float blendVividLight(float base, float blend) {
	return (blend<0.5)?blendColorBurn(base,(2.0*blend)):blendColorDodge(base,(2.0*(blend-0.5)));
}

vec4 blendVividLight(vec4 base, vec4 blend) {
	return vec4(blendVividLight(base.r,blend.r),blendVividLight(base.g,blend.g),blendVividLight(base.b,blend.b),blendVividLight(base.a,blend.a));
}
vec4 over(vec4 base, vec4 blend) {
	return vec4(mix(base, blend, 1-base.a));
}


//
//Vector math functions
//
vec3 limitMag(vec3 inputVec, float maxMag)
{
    vec3 outputVec = inputVec;
    float vecLength = length(inputVec);
    if (vecLength > maxMag && vecLength > 0)
    {
        float ratio = maxMag/vecLength;
        outputVec *= ratio;
    }
    
    return outputVec;
}
vec3 setMag(vec3 inputVec, float magnitude)
{
    vec3 normVec = normalize(inputVec);
    vec3 outputVec = normVec * magnitude;
	
	return outputVec;
}


//
//UV functions
//
vec2 position2DToWorldSpaceUV(vec3 position, vec3 uMinWorld, vec3 uMaxWorld)
{
	vec2 outUv = clamp((position - uMinWorld) / (uMaxWorld - uMinWorld), 0.0, 1.0).xy;
	return outUv;
}

//
//Other functions
//
float remap(float value, float min1, float max1, float min2, float max2)
{
	return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}
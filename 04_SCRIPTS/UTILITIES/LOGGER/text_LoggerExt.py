"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions

import os
import logging, sys
from logging.handlers import RotatingFileHandler

class LoggerExt:
	"""
	LoggerExt description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp
		
		loggerConfig = root.Config['Logger Settings']
		self.ownerComp.par.Logfolder = loggerConfig['Log folder'] if loggerConfig != '' else self.ownerComp.par.Logfolder.default
		self.ownerComp.par.Keeplogfiles = loggerConfig['Keep log files'] if loggerConfig != '' else self.ownerComp.par.Keeplogfiles.default
		self.ownerComp.par.Loginfotofile = loggerConfig['Log info to file'] if loggerConfig != '' else self.ownerComp.par.Loginfotofile.default
		self.ownerComp.par.Loginleveltofile = loggerConfig['Login level to file'] if loggerConfig != '' else self.ownerComp.par.Loginleveltofile.default
		
		if 'TD_LOG_SUFFIX' in os.environ:
			self.ownerComp.par.Suffix = os.environ['TD_LOG_SUFFIX']
		elif loggerConfig['Suffix'] != '':
			self.ownerComp.par.Suffix = loggerConfig['Suffix']
		else:
			self.ownerComp.par.Suffix = self.ownerComp.par.Suffix.default

		# Initialize Loggers
		# Console
		self.consoleLogger = logging.getLogger('consoleLogger')
		self.consoleLogger.setLevel(logging.DEBUG)

		# File
		self.fileLogger = logging.getLogger('fileLogger')

		self.fileLogger.setLevel(logging.INFO) if self.ownerComp.par.Loginfotofile else self.fileLogger.setLevel(logging.WARNING)

		# Setting up log folder if doesn't exist
		pathToLogFolder = self.ownerComp.par.Logfolder.eval() + '/'
		if not os.path.exists(pathToLogFolder):
			os.makedirs(pathToLogFolder)

		fileName = project.name.split('.')[0] + '_' + self.ownerComp.par.Suffix.eval() if self.ownerComp.par.Suffix.eval() != '' else project.name.split('.')[0]
		filesHandler = RotatingFileHandler(pathToLogFolder + fileName + '.log', mode='a', maxBytes=10*1024*1024, backupCount=self.ownerComp.par.Keeplogfiles.eval())
		ch = logging.StreamHandler(sys.stdout)

		filesHandler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', '%Y-%m-%dT%H:%M:%S%z'))
		ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', '%Y-%m-%dT%H:%M:%S%z'))

		# add the handlers to the logger
		self.fileLogger.addHandler(filesHandler)
		self.consoleLogger.addHandler(ch)

	def Log(self, val, level='DEBUG'):
		if level == 'DEBUG':
			self.ownerComp.Debug(val)
		
		elif level == 'INFO':
			self.ownerComp.Info(val)
		
		elif level == 'WARNING':
			self.ownerComp.Warning(val)
		
		elif level == 'ERROR':
			self.ownerComp.Error(val)

		return

	def Debug(self, val):
		caller = inspect.stack()
		callerOp = caller[1][1]
		callerLine = caller[1][2]

		val = '(' + str(callerOp) + ' line:' + str(callerLine)+ ') ' + str(val)
		
		if op.FRAMEWORK_MANAGER.par.Mode != 'Production':
			self.consoleLogger.info(val)

		return

	def Info(self, val):
		caller = inspect.stack()
		callerOp = caller[1][1]
		callerLine = caller[1][2]

		val = '(' + str(callerOp) + ' line:' + str(callerLine)+ ') ' + str(val)
		self.fileLogger.info(val)
		
		if op.FRAMEWORK_MANAGER.par.Mode != 'Production':
			self.consoleLogger.info(val)
		
		return

	def Warning(self, val):
		caller = inspect.stack()
		callerOp = caller[1][1]
		callerLine = caller[1][2]

		val = '(' + str(callerOp) + ' line:' + str(callerLine)+ ') ' + str(val)
		self.fileLogger.warning(val)
		
		if op.FRAMEWORK_MANAGER.par.Mode != 'Production':
			self.consoleLogger.warning(val)
				
		return

	def Error(self, val):
		caller = inspect.stack()
		callerOp = caller[1][1]
		callerLine = caller[1][2]

		val = '(' + str(callerOp) + ' line:' + str(callerLine)+ ') ' + str(val)
		self.fileLogger.error(val)
		
		if op.FRAMEWORK_MANAGER.par.Mode != 'Production':
			self.consoleLogger.error(val)
		
		return
	
	def UpdateFileLogLevel(self):
		self.fileLogger.setLevel(int(parent().par.Loginleveltofile))

	def GetFileLoggerLoggingLevel(self):
		print(self.fileLogger.getEffectiveLevel())

